﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class LevelData
{

    public static List<int[,]> levelData = new List<int[,]>();
    public static List<int[]> levelCompleteData = new List<int[]>();
    public static List<int> levelComplete = new List<int>();


    public static void GenerateLevelsData()
    {
        int[,] level1 = {   {-1, 0, 2, 4,-1},
                            {-1, 1,-1, 3,-1},
                            {-1,-1,-1,-1,-1},
                            {-1,-1, 2,-1,4 },
                            { 0,-1, 1,-1,3}};
        levelData.Add(level1);

        int completeCnt1 = 15;

        levelComplete.Add(completeCnt1);

        int[,] level2 = {   { 2, 3,-1,-1, 3},
                            {-1,-1,-1, 2, 4},
                            { 1, 4,-1,-1,-1},
                            {-1,-1,-1, 1, 0},
                            { 0,-1,-1,-1,-1}};
        levelData.Add(level2);

        int completeCnt2 = 15;

        levelComplete.Add(completeCnt2);


        int[,] level3 = { {   3,-1,-1,-1,-1},
                            {-1,-1,-1,-1,-1},
                            {-1,-1, 1,-1,-1},
                            { 2, 1, 0,-1, 3},
                            { 0,-1,-1,-1, 2}};
        levelData.Add(level3);

        int completeCnt3 = 17;

        levelComplete.Add(completeCnt3);


        int[,] level4 = {{1,2,3,-1,-1},
                            {-1,-1,-1,2,-1},
                            {-1,-1,3,-1,-1},
                            {0,-1,-1,-1,-1},
                            {-1,-1,-1,0,1}
                        };
        levelData.Add(level4);

        int completeCnt4 = 17;

        levelComplete.Add(completeCnt4);

        int[,] level5 = {{-1,-1,0,-1,3},
                            {-1,-1,-1,-1,2},
                            {-1,-1,-1,-1,-1},
                            {-1,3,2,1,-1},
                            {-1,-1,-1,0,1}
                        };
        levelData.Add(level5);

        int completeCnt5 = 17;

        levelComplete.Add(completeCnt5);

        int[,] level6 = {{-1,-1,-1,-1,3},
                            {-1,2,-1,-1,0},
                            {-1,-1,-1,1,-1},
                            {-1,-1,-1,-1,-1},
                            {3,-1,2,0,1}};
        levelData.Add(level6);

        int completeCnt6 = 17;

        levelComplete.Add(completeCnt6);



        int[,] level7 = {{-1, 2, 1, 0,-1},
                         {-1,-1,-1,-1,-1},
                         {-1,-1, 1,-1,-1},
                         {-1,-1,-1, 4,-1},
                         { 2, -1, 4, 0,-1}};
        levelData.Add(level7);

        int completeCnt7 = 17;

        levelComplete.Add(completeCnt7);


        int[,] level8 = {{-1,-1, 1,-1,-1},
                         {-1, 2, -1,-1,1},
                         {-1,-1, 0,-1,-1},
                         {-1,-1,-1,-1,-1},
                         { 2,-1,-1, 0,-1}};
        levelData.Add(level8);

        int completeCnt8 = 19;

        levelComplete.Add(completeCnt8);



        int[,] level9 = {{ 3,-1,-1,-1,-1},
                         {-1,-1,-1, 3,-1},
                         {-1, 6, 1, 6, 2},
                         {-1, 2,-1,-1,-1},
                         { 1,-1,-1,-1,-1}};
        levelData.Add(level9);

        int completeCnt9 = 17;

        levelComplete.Add(completeCnt9);



        int[,] level10 = {{-1,-1, 0,-1, -1},
                          {-1, 1, 6,-1,-1},
                          {-1,-1, 1,-1,-1},
                          {-1,-1, 6, 2,-1},
                          { 2,-1,-1,-1, 0}};
        levelData.Add(level10);

        int completeCnt10 = 17;

        levelComplete.Add(completeCnt10);

      

        int[,] level11 = {{ 1,-1,-1,3,-1},
                          { 0, 6,-1, 3,-1},
                          {-1, 0,-1,-1,-1},
                          {-1,-1, 2, 6,-1},
                          { 2, -1, -1, 1,-1}};
        levelData.Add(level11);

        int completeCnt11 = 15;

        levelComplete.Add(completeCnt11);


        int[,] level12 = {{-1,-1,-1,-1,-1},
                          { 1,-1,-1,2,-1},
                          { 6,-1,-1,-1,-1},
                          {-1,-1,-1,-1,-1},
                          {-1, 2,-1,-1, 1}};
        levelData.Add(level12);

        int completeCnt12 = 20;

        levelComplete.Add(completeCnt12);

        

        int[,] level13 = {{ 4,-1,-1, 2,-1,-1},
                          {-1,-1,-1, 2,-1,-1},
                          {-1,-1,-1, 0,-1,-1},
                          {-1,-1, 4, 1,-1,-1},
                          { 3,-1,-1, 1,-1,-1},
                          { 3,-1,-1,-1,-1, 0}};
        levelData.Add(level13);

        int completeCnt13 = 26;

        levelComplete.Add(completeCnt13);



        int[,] level14 = {{-1,-1,-1,-1,-1, 4},
                          {-1, 0,-1,-1, 4,-1},
                          { 3,-1, 3,-1,-1,-1},
                          {-1,-1,-1,-1,-1,-1},
                          {-1, 0,-1,-1,-1, 2},
                          {-1,-1,-1,-1,-1, 2}};
        levelData.Add(level14);


        int completeCnt14 = 28;
        levelComplete.Add(completeCnt14);




        int[,] level15 = {{-1,-1, 4,-1,-1, 4},
                          {-1, 0,-1,-1,-1, 3},
                          {-1,-1,-1, 0,-1,-1},
                          {-1,-1,-1,-1, 3,-1},
                          {-1, 2,-1,-1,-1,-1},
                          {-1, 2, 1,-1,-1, 1}};
        levelData.Add(level15);


        int completeCnt15 = 26;
        levelComplete.Add(completeCnt15);


        int[,] level16 = {{-1,-1,-1,-1, 2, 4},
                          {-1, 2,-1,-1, 6,-1},
                          { 3,-1,-1,-1, 4,-1},
                          {-1,-1, 6,-1,-1,-1},
                          {-1,-1,-1,-1,-1, 3},
                          {-1,-1,-1, 1,-1, 1}};
        levelData.Add(level16);


        int completeCnt16 = 26;
        levelComplete.Add(completeCnt16);



        int[,] level17 = {{-1,-1,-1,-1, 2, 4},
                          {-1, 2,-1,-1, 6,-1},
                          {-1,-1,-1,-1, 4,-1},
                          { 6, 3,-1,-1,-1,-1},
                          {-1,-1,-1,-1,-1,-1},
                          { 3,-1,-1, 1,-1, 1}};
        levelData.Add(level17);


        int completeCnt17 = 26;
        levelComplete.Add(completeCnt17);

        int[,] level18 = {{ 1,-1,-1,-1, 6, 1},
                          { 0,-1, 2,-1,-1,-1},
                          {-1,-1,-1, 2,-1,-1},
                          {-1,-1,-1, 6,-1,-1},
                          {-1, 0,-1,-1,-1,-1},
                          {-1,-1, 4,-1,-1, 4}};
        levelData.Add(level18);


        int completeCnt18 = 26;
        levelComplete.Add(completeCnt18);




        int[,] level19 = {{-1,-1,-1,-1,-1, 3},
                          {-1, 2,-1, 1,-1,-1},
                          {-1,-1,-1,-1,-1,-1},
                          {-1,-1, 3,-1,-1,-1},
                          {-1,-1,-1,-1,-1, 1},
                          {-1,-1,-1,-1,-1, 2}};
        levelData.Add(level19);


        int completeCnt19 = 30;
        levelComplete.Add(completeCnt19);



        int[,] level20 = {{ 0,-1, 2,-1,-1,-1},
                          {-1,-1, 2,-1,-1,-1},
                          {-1,-1,-1,-1,-1,-1},
                          {-1,-1,-1,-1,-1,-1},
                          {-1,-1, 1,-1,-1, 6},
                          {-1,-1, 1,-1,-1, 0}};
        levelData.Add(level20);


        int completeCnt20 = 29;
        levelComplete.Add(completeCnt20);


        int[,] level21 = {{-1,-1,-1,-1,-1, 3},
                          {-1,-1,-1, 1,-1,-1},
                          {-1,-1,-1,-1,-1,-1},
                          {-1,-1, 3,-1,-1,-1},
                          {-1,-1,-1,-1,-1,-1},
                          {-1,-1,-1,-1,-1, 1}};
        levelData.Add(level21);



        int completeCnt21 = 32;
        levelComplete.Add(completeCnt21);


        int[,] level22 = {{ 3,-1,-1,-1,-1,-1},
                          {-1,-1,-1,-1, 1,-1},
                          {-1,-1,-1,-1,-1,-1},
                          {-1,-1,-1,-1, 6, 6},
                          {-1,-1,-1,-1,-1,-1},
                          { 3,-1,-1,-1,-1, 1}};
        levelData.Add(level22);



        int completeCnt22 = 30;
        levelComplete.Add(completeCnt22);



        int[,] level23 = {{-1,-1,-1,-1, 1, 3},
                          {-1, 0,-1,-1,-1,-1},
                          {-1, 2, 2,-1,-1,-1},
                          {-1,-1,-1,-1,-1,-1},
                          { 1, 0,-1,-1,-1,-1},
                          { 3,-1,-1,-1,-1,-1}};
        levelData.Add(level23);



        int completeCnt23 = 28;
        levelComplete.Add(completeCnt23);



        int[,] level24 = {{ 1, 6,-1,-1,-1,-1},
                          {-1,-1,-1,-1,-1,-1},
                          { 6,-1,-1, 6,-1,-1},
                          { 1,-1,-1,-1,-1, 2},
                          {-1,-1,-1,-1,-1,-1},
                          { 2,-1,-1, 6,-1,-1}};
        levelData.Add(level24);



        int completeCnt24 = 28;
        levelComplete.Add(completeCnt24);




        int[,] level25 = {{-1,-1, 4,-1,-1,-1},
                          {-1, 2,-1,-1,-1,-1},
                          {-1,-1,-1,-1,-1, 4},
                          {-1, 1, 6,-1,-1, 2},
                          {-1,-1,-1,-1,-1,-1},
                          { 1,-1,-1,-1,-1,-1}};
        levelData.Add(level25);



        int completeCnt25 = 29;
        levelComplete.Add(completeCnt25);
    }

}
