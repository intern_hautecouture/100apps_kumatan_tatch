﻿using UnityEngine;
using System.Collections;
//using NendUnityPlugin.AD;
//using NendUnityPlugin.Common;

public class InstructionScript : MonoBehaviour {

    public AudioClip buttonClickSound;
    //private static int i;
	[System.NonSerialized]
	public bool playing = false;
    public GameObject LB;
    public GameObject buttonGuardObject;
    private GameObject buttonGuardInstance;
    void Start()
    {
        //LevelButton levelbutton = LB.GetComponent<LevelButton>();
    }
    void Awake()
    {
        //StartCoroutine(LoadAd());
    }

	void OnEnable()
	{
		GameManager.Instance.CurrentScene = Constants.INSTRUCTION_SCENE;
        //NendAdInterstitial.Instance.Show();

		MainMenuScript.instance.instructionText.gameObject.SetActive (true);
	}

    void OnBackButtonClick()
    {
        if (GameManager.Instance.IsSound)
			AudioSource.PlayClipAtPoint(buttonClickSound, new Vector3(), 
			                            MainMenuScript.instance.volumeSE);   
        StartCoroutine(BackButtonAction());


    }

    private IEnumerator BackButtonAction()
    {
        buttonGuardInstance = Instantiate(buttonGuardObject, new Vector3(0, 0, -6), Quaternion.identity) as GameObject;
        yield return new WaitForSeconds(0.5f);
        Destroy(buttonGuardInstance);

        MainMenuScript.instance.instructionText.gameObject.SetActive (false);

        Camera.main.SendMessage("activeInstructionObject", false, SendMessageOptions.RequireReceiver);

		if (!playing) {
			Camera.main.SendMessage("activeMainMenuObject", true, 
			                        SendMessageOptions.RequireReceiver);
		} else {
            //LevelButton levelbutton = LB.GetComponent<LevelButton>();
            if (PlayerPrefs.GetInt("Level") == 0 && LevelButton.tutorial==true){
                LevelButton.tutorial = false;
                //すでにレベルは設定されている
                var levelObject = Instantiate(GameManager.Instance.levelObject, 
				            new Vector3(0f, 0f, 0f), Quaternion.identity);
                levelObject.GetComponent<GameController>().Timer = 31f;
			}else{
				Camera.main.SendMessage("activeMainMenuObject", true, 
				                        SendMessageOptions.RequireReceiver);
			}

		}
        
    }

	/*
    private IEnumerator LoadAd()
    {
        Debug.Log("ddddddddddddddddddd");

        string apiKey = "";
        string spotId = "";

#if UNITY_IPHONE
		apiKey = "1f1b9a3298c32d522703bfb674b9c3907f1436a8";
		spotId = "397831";
		Handheld.SetActivityIndicatorStyle(iOSActivityIndicatorStyle.Gray);
#elif UNITY_ANDROID
        apiKey = "d22d63f50bcfd9f78fbe0dbec49bea5c37f1120d";
        spotId = "397838";
        Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
#endif

        Handheld.StartActivityIndicator();
        yield return new WaitForSeconds(0.0f);

        NendAdInterstitial.Instance.Load(apiKey, spotId);
    }
	*/
}
