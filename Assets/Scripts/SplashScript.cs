﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashScript : MonoBehaviour {

    public GameObject canvas;
    public AudioSource backgroundSource;
	public RawImage splashImg;
	public Texture[] sprites;

	void Awake()
	{
		#if UNITY_IOS
		string iosGeneration = UnityEngine.iOS.Device.generation.ToString();
		float aspectRatio9_16 = 9 / 16.0f;
		//check for iphoneX
		if(iosGeneration.Contains("X") && (Camera.main.aspect != aspectRatio9_16)) 
		{
			//change sprite to ios_X 
			Debug.Log("iphone X");
			splashImg.texture = sprites[0];
			//splashImg.SetNativeSize();
		}
		//check for ipad
		else if (iosGeneration.Contains("iPad"))
		{
			//change sprite to ios_3
			Debug.Log("ipad");
			splashImg.texture = sprites[3];
		}
		//for ios6,7,8
		else if(iosGeneration.Contains("6") || iosGeneration.Contains("7") || iosGeneration.Contains("8"))
		{
			splashImg.texture = sprites[2];
		}
		//for any other version of ios (less than 6)
		//and for iphone X and XS with aspect ratio of 9:16
		else
		{
			//change sprite to ios_1
			splashImg.texture = sprites[1];
		}
		#endif
	}

	// Use this for initialization
	void Start () 
	{
		Debug.Log (9 / 16.0f);
		Debug.Log (Camera.main.aspect);

		StartCoroutine(ShowMenu());
	}
	
    IEnumerator ShowMenu()
    {
        yield return new WaitForSeconds(1.5f);
        Debug.Log("showmenu");
        backgroundSource.Play();
        canvas.SetActive(true);
        gameObject.SetActive(false);
    }
}
