﻿using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour
{
    GameObject gameControllerInstance;

	static public int translate = -170;
	static public int translate_h = 8;

    void OnEnable()
    {
        gameControllerInstance = GameObject.FindGameObjectWithTag(Constants.TAG_GAME_CONTROLLER);
        GameManager.Instance.ResetGameManager();
        //CellPosition.CalculateCellPosition(Constants.SIX_GRID_SIZE);
        LevelData.GenerateLevelsData();
        CreateLevelObjects();

		//追加(2015/8/26 原田貴広)==================================
		foreach(Transform childTrfm in transform){

			if(childTrfm.name != "GameBgSprite"){
				childTrfm.Translate(new Vector3(0, translate, 0));

			}

			//childTrfm.Translate(new Vector3(translate_h, 0, 0));
		}
		//=========================================================ここまで

		transform.Translate (new Vector3(translate_h, 0, 0));
    }

    private void CreateLevelObjects()
    {
        int[,] levelData = LevelData.levelData[GameManager.Instance.LevelId - 1];

        float idx = 0;

        CellPosition.CalculateCellPosition(levelData.Length);

        for (int i = 0; i < levelData.GetLength(0); i++)
        {
            for (int j = 0; j < levelData.GetLength(1); j++)
            {
                int cellId = levelData[i, j] == -1 ? 5 : levelData[i, j];
          
               
                GameObject cellObject = gameControllerInstance.GetComponent<ObjectCollection>().cellType[cellId];


                switch (levelData.Length)
                {
                    case 25://5マス
                        idx = 1f;
                        break;
                    case 36://6マス
                        idx = 0.833f;
                        break;
                }


                cellObject.transform.localScale = new Vector3(idx,idx, 1f);

                GameObject cellInstance = Instantiate(cellObject, CellPosition.cell_xy[i, j], Quaternion.identity) as GameObject;
				cellInstance.transform.Translate(new Vector3(1.5f * translate_h, 0, 0));

                cellInstance.transform.parent = gameObject.transform;
                cellInstance.GetComponent<Cell>().RowIndex = i;
                cellInstance.GetComponent<Cell>().ColIndex = j;
            }
        }
    }
}
