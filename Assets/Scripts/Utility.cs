﻿using UnityEngine;
using System.Collections;
using System;

public class Utility : MonoBehaviour
{

    void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    public static int CalculateAspectRatioIndex()
    {
        double aspectRatioCurrent = 0f;
        if (IsLandscape())
            aspectRatioCurrent = Math.Round(Screen.width * 1f / Screen.height, 2);
        else
            aspectRatioCurrent = Math.Round(Screen.height * 1f / Screen.width, 2);

        double[] designAspectRatio = new double[5];
        designAspectRatio[CameraConstants.ASPECTRATIO_3TO2] = Math.Round(CameraConstants.ASPECTRATIO_3TO2_WIDTH / CameraConstants.ASPECTRATIO_3TO2_HEIGHT, 2);
        designAspectRatio[CameraConstants.ASPECTRATIO_16TO9] = Math.Round(CameraConstants.ASPECTRATIO_16TO9_WIDTH / CameraConstants.ASPECTRATIO_16TO9_HEIGHT, 2);
        designAspectRatio[CameraConstants.ASPECTRATIO_5TO3] = Math.Round(CameraConstants.ASPECTRATIO_5TO3_WIDTH / CameraConstants.ASPECTRATIO_5TO3_HEIGHT, 2);
        designAspectRatio[CameraConstants.ASPECTRATIO_8TO5] = Math.Round(CameraConstants.ASPECTRATIO_8TO5_WIDTH / CameraConstants.ASPECTRATIO_8TO5_HEIGHT, 2);
        designAspectRatio[CameraConstants.ASPECTRATIO_4TO3] = Math.Round(CameraConstants.ASPECTRATIO_4TO3_WIDTH / CameraConstants.ASPECTRATIO_4TO3_HEIGHT, 2);

        double currentNearest = designAspectRatio[CameraConstants.ASPECTRATIO_3TO2];
        double currentDifference = Mathf.Abs((float)aspectRatioCurrent - (float)currentNearest);
        int currentNearestIndex = 0;

        for (int i = 1; i < designAspectRatio.Length; i++)
        {
            double diff = Mathf.Abs((float)designAspectRatio[i] - (float)aspectRatioCurrent);
            if (diff < currentDifference)
            {
                currentDifference = diff;
                currentNearest = designAspectRatio[i];
                currentNearestIndex = i;
            }
        }

        return currentNearestIndex;
    }

    public static bool IsLandscape()
    {
        if (Screen.width > Screen.height)
            return true;

        return false;
    }

    public static bool IsSameAspectRatio()
    {
        double aspectRatioCurrent = 0f;
        if (IsLandscape())
            aspectRatioCurrent = Math.Round(Screen.width * 1f / Screen.height, 2);
        else
            aspectRatioCurrent = Math.Round(Screen.height * 1f / Screen.width, 2);

        double aspectRatioDesign = Math.Round(CameraConstants.ASPECTRATIO_16TO9_WIDTH / CameraConstants.ASPECTRATIO_16TO9_HEIGHT,2);
        if (aspectRatioDesign == aspectRatioCurrent)
            return true;

        return false;
    }

    public static bool IsLandscapeByOrientation()
    {
        if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight)
            return true;

        return false;
    }


}
