﻿using UnityEngine;
using System.Collections;


public class ReviewPanelScript : MonoBehaviour {
	
	GameObject BackButton, RetryButton, NextLevelButton, stars;	//to be deactivated
	GameObject reviewStar1, reviewStar2, reviewStar3, reviewStar4, reviewStar5, SendReviewButton, dialogueBG;		//to be activated
	GameObject toggleReview, continueButton, instructionButton, newGameButton, googleReviewMainButton;

	GameObject backBTN, nextBTN, replayBTN;			// buttons On GameOver

	void mainReviewButtonToggle () {


	}
	void Start()
	{

		continueButton = GameObject.Find ("Continue");
		instructionButton = GameObject.Find ("InstructionsBtnSprite");
		newGameButton = GameObject.Find ("PlayBtnSprite");

		backBTN = GameObject.Find ("backbtn");
		nextBTN = GameObject.Find ("NextLevelBtnSprite");
		replayBTN = GameObject.Find ("RestartBtnSprite");

		googleReviewMainButton = GameObject.Find ("googleReviewMainButton");

	}

	void Awake()
	{
   
		SendReviewButton = GameObject.Find ("SendReview");		//the whole dialogue box containing all rating panel elements

		if(SendReviewButton != null)
			SendReviewButton.SetActive (false);
	}

	public void DeactivatateReviewButtons()
	{

	}

	public void DeactivatateReviewButtonsFromDialogueBox()
	{
		if(SendReviewButton != null)
		SendReviewButton.SetActive (false);
		if (continueButton != null)
			continueButton.SetActive (true);
		if (instructionButton != null)
			instructionButton.SetActive (true);
		if (newGameButton != null)
			newGameButton.SetActive (true);

		if (backBTN != null)
			backBTN.SetActive (true);
		if (nextBTN != null)
			nextBTN.SetActive (true);
		if (replayBTN != null)
			replayBTN.SetActive (true);

		if (googleReviewMainButton != null) {
			googleReviewMainButton.GetComponent<tk2dUIItem> ().SendMessageOnClickMethodName = "ActiveReviewPanel";

			googleReviewMainButton.SetActive (true);
		}
	}

	public void facebookLike()
	{

		Application.OpenURL ("https://www.facebook.com/miraclestudiosgames?fref=ts");
	}

	public void ActiveReviewPanel () {

		if(SendReviewButton != null)
			SendReviewButton.SetActive (true);
		if (continueButton != null)
			continueButton.SetActive (false);
		if (instructionButton != null)
			instructionButton.SetActive (false);
		if (newGameButton != null)
			newGameButton.SetActive (false);

		if (backBTN != null)
			backBTN.SetActive (false);
		if (nextBTN != null)
			nextBTN.SetActive (false);
		if (replayBTN != null)
			replayBTN.SetActive (false);

		if (googleReviewMainButton != null) {
			googleReviewMainButton.GetComponent<tk2dUIItem> ().SendMessageOnClickMethodName = "DeactivatateReviewButtonsFromDialogueBox";

			googleReviewMainButton.SetActive (false);
		}
	}


	public void reactiveReviewDialogue() {
		if(SendReviewButton == null)
		SendReviewButton.SetActive (true);
	}



}
