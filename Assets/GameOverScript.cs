﻿using UnityEngine;
using System.Collections;
using NendUnityPlugin.AD;
//using NendUnityPlugin.Common;


public class GameOverScript : MonoBehaviour {

    public AudioClip clickAudioClip;
    public GameObject buttonGuardObject;
    private GameObject buttonGuardInstance;

    void Awake()
    {
        //StartCoroutine(LoadAd());
    }


    void OnEnable()
    {
        GameManager.Instance.IsLevelCompleteShowing = true;
		if(Random.Range(0f,1f) < 0.3f ) 
        	NendAdInterstitial.Instance.Show();

		//admob interstitial
		//AdMobBannerInterstitial.Instance.ShowBanner ();
		//AdmobManager.ShowInterstitial ();

		GameManager.Instance.CurrentScene = Constants.GAME_OVER_SCENE;

		//backButtonClicked = false;
    }


	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) {
			//if(!backButtonClicked)
			{
				//backButtonClicked = true;
				BackButtonClick();
			}
		}
	}


	//bool backButtonClicked = false;
	void BackButtonClick()
	{

		StartCoroutine (BackButtonAction());
	}
	private IEnumerator BackButtonAction()
	{
        buttonGuardInstance = Instantiate(buttonGuardObject, new Vector3(0, 0, -6), Quaternion.identity) as GameObject;
        yield return new WaitForSeconds(0.5f);
        Destroy(buttonGuardInstance);

        //古いステージは消す
        Destroy(GameObject.Find("LevelObject(Clone)"));

		//レベルセレクト画面を有効にする
		Camera.main.SendMessage("activeLevelSelectObject", true, 
		                        SendMessageOptions.RequireReceiver);

		//自分を無効にする
		//Camera.main.SendMessage("activeGameOverObject", false, 
		//                        SendMessageOptions.RequireReceiver);
		gameObject.SetActive (false);
	}

    void RestartButtonClick()
    {
        Debug.Log("Replay");

        if (GameManager.Instance.IsSound)
            AudioSource.PlayClipAtPoint(clickAudioClip, new Vector3(), 
			                            MainMenuScript.instance.volumeSE);
        StartCoroutine(RestartAction());
    }

    private IEnumerator RestartAction()
    {
        Debug.Log("Restart");
        Time.timeScale = 1f;
        buttonGuardInstance = Instantiate(buttonGuardObject, new Vector3(0, 0, -6), Quaternion.identity) as GameObject;
        yield return new WaitForSeconds(0.5f);
        Destroy(buttonGuardInstance);
        Destroy(GameObject.Find("LevelObject(Clone)"));
        LoadLevel();
    }

    private void LoadLevel()
    {
        var levelObject = Instantiate(GameManager.Instance.levelObject, new Vector3(0, 0, 0), Quaternion.identity);
        levelObject.GetComponent<GameController>().Timer = 31f;
        Camera.main.SendMessage("activeGameOverObject", false, SendMessageOptions.RequireReceiver);
    }

    void OnDisable()
    {
        GameManager.Instance.IsLevelCompleteShowing = false;
    }





    
}
